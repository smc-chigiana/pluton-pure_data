notes on porting pluton from ISPW max to Pd.

1. "portage" directory.  Extract the "explodes" from Pluton.pat

2. "dummy" directory.  Some objects have to be changed by hand:

declare -- delete it.

table~ -- replace by "table" multiplying lengths by 44.1.  Read messages
replaced by messages to soundfiler.

sampread~ -- replace by tabread4~; multiply inputs by 44.1

sampwrite~ -- replace by tabwrite~

osc1~ in gumbo -- replace with tabosc4~

down~, up~ -- don't exist; deal with these ad hoc

--------------------------------

new dolist:
number of output channels controlled by switch
add MIDI level controls
remake control panels with sliders

------------------------
patch labels and score rehearsal points
First column is rehearsal point in score
Second column is patch section number (with event number if not at start)
page numbers are from published score, not manuscript

I     1
IB    1/11  
II.A  2     p8  (osc + mkov)
II.D  2/10  p11
II.E  2/22  p14
II.F  3     p17
III   21    p24  (Mkov)
      22    top p26, no electronic sounds
IV.A  31    bottom P26  (oscs)
IV.B  32    p31
IV.C  33    p33
IV.D  34    p36
V.A   40    p40 no electronic sounds
V.B   41    p44
V.D   42    P49
V.E   43    p52
V.F   44    p58

