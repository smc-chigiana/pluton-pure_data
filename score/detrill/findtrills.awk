BEGIN   {a = b = c = d = e = f = trill = trilla = trillb = 0
        trillcount = 100}
        {
            if (($2 == b || $2 == c) && (a == c || a == d) && b == d && c == e && d == f) {
                trill = 1;
                trilla = d;
                trillb = e;
            }
            if (trill && ($2 != trilla) && ($2 != trillb)) {
                trill = trillcount = 0;
            }
            if ((!trill) && trillcount < 5 && ($2 == trilla || $2 == trillb)) {
                 $4 = "FUZZ";
            }
            if (($2 == trilla || $2 == trillb) && !trill) {
                trillcount = trillcount - 3;
            }
            if (($2 != trilla && $2 != trillb) && !trill) {
                trillcount = trillcount + 2;
            }
            if (trill) $4 = "TRILL";
            f = e;
            e = d;
            d = c;
            c = b;
            b = a;
            a = $2;
            if (($4 == "TRILL" || $4 == "FUZZ") && $3 != 0) {
                $5 = "BAD"
            }
            print
        }
