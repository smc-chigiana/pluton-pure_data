#include "m_pd.h"
#include <stdlib.h>
#include <stdio.h>

#define DIM 88
#define FIRST 21

typedef struct _element
{
    float t_prob;
    float t_vel;
    float t_length;
    float t_delay;
} t_element;

typedef struct _hang
{
    t_element *h_element;
    double h_time;
} t_hang;

typedef struct _snarkov
{
    	    /* pd stuff */
    t_object x_ob;
    t_clock *x_clock;
    t_outlet *x_pitchout;
    t_outlet *x_velout;
    t_outlet *x_durout;
    t_canvas *x_canvas;
    float x_vel;
    	    /* matrix stuff */
    t_element x_matrix[DIM][DIM];
    t_hang x_hang[DIM];     /* elements which have no duration yet */
    t_element *x_playtrans; /* next matrix transition to play back */
    int x_playpit;  	    /* next pitch to play back */
    int x_record;   	    /* true if "record" is on */
    int x_recpit;   	    /* previous pitch recorded */
    double x_rectime;	    /* time of previous note recorded */
    int x_firstpit; 	    /* pitch of first note recorded */
    float x_firstvel;	    /* its velocity */
    float x_firstlength;    /* its duration (msec) */
    double x_firsttime;     /* time first recorded note-on arrived */
    int x_chordlength;	    /* number of notes so far played as chord */
    	    /* parameters */
    float x_resttime;
    float x_maxdiff;
    int x_restart;
} t_snarkov;

static void snarkov_float(t_snarkov *x, t_float f)
{
    int x1 = ((int)f) - FIRST;
    float vel = x->x_vel;
    int lastrec;
    float timediff;

    if (!x->x_record || x1 < 0 || x1 >= DIM)
        return;

    if (vel != 0)
    {
	if (x->x_firstpit < 0)
	{
	   x->x_firstpit = x1;
	   x->x_firstvel = vel;
	   x->x_firstlength = 1000;
	   x->x_firsttime = clock_getsystime();
	}
	else if ((timediff = clock_gettimesince(x->x_rectime))
	    < x->x_resttime)
	{
	    t_element *t = &x->x_matrix[x->x_recpit][x1];
	    if (x->x_recpit < 0 || x->x_recpit >= DIM)
	    {
	    	pd_error(x, "snarkov bug 2");
		return;
	    }	
	    t->t_vel = vel;
	    t->t_prob = vel;
	    t->t_length = 1000;
	    if (timediff < x->x_maxdiff)
	    	timediff = 0;
	    t->t_delay = timediff;
	    x->x_hang[x1].h_time = clock_getlogicaltime();
	    x->x_hang[x1].h_element = t;
	}
	x->x_recpit = x1;
	x->x_rectime = clock_getsystime();
    }
    else    /* note off: find corresponding note on and record duration */
    {
	t_hang *h = &x->x_hang[x1];
	if (x->x_firsttime != 0)
	{
	   x->x_firstlength = clock_gettimesince(x->x_firsttime);
	   x->x_firsttime = 0;
	}
	if (h->h_element) 
	{
	    float del = clock_gettimesince(h->h_time);
	    if (del > 10000) del = 10000;
	    else if (del < 1) del = 1;
	    h->h_element->t_length = del;
	    h->h_element = 0;
	}
	return;
    }
}

void snarkov_clear(t_snarkov *x)
{
    int i, j;

    x->x_record = 0;
    for (i=0; i<DIM; i++)
    	for (j=0; j<DIM; j++)
    {
    	x->x_matrix[i][j].t_prob = 0;
    	x->x_matrix[i][j].t_vel = 0;
    	x->x_matrix[i][j].t_length = 0;
    	x->x_matrix[i][j].t_delay = 0;
    }
    for (i=0; i<DIM; i++)
    	x->x_hang[i].h_element = 0;
    x->x_playtrans = 0;
    x->x_playpit = 0;
    x->x_recpit = 0;
    x->x_rectime = 0;
    x->x_firstpit = -1;
    x->x_firstvel = 0;
    x->x_firstlength = 0;
    x->x_firsttime = 0;
    x->x_chordlength = 0;
    clock_unset(x->x_clock);
}

static void snarkov_record(t_snarkov *x)
{
    x->x_record = 1;
}

static void snarkov_stoprec(t_snarkov *x)
{
    x->x_record = 0;
}

static void snarkov_next(t_snarkov *x)
{
    int i, row;
    float sum = 0, r, rats;

    row = x->x_playpit;
    for (i = 0; i < DIM; i++)
    	sum += x->x_matrix[row][i].t_prob;

    if (sum <= 0)
    	goto fail;
    {
	static int randphase;
	randphase = 459879827 * randphase + 845793723;
	rats = sum * ((float)((randphase >> 5) & 32767))/32768.;
    }
    for (i=0; i < DIM; i++)
    {
	rats -= x->x_matrix[row][i].t_prob;
	if (rats <= 0)
	    break;
    }
    if (i == DIM)
    {
    	pd_error(x, "snarkov bug");
    	goto fail;
    }
    x->x_playtrans = &x->x_matrix[row][i];
    x->x_playpit = i;
    if (x->x_playtrans->t_delay < 1)
    	x->x_chordlength++;
    else x->x_chordlength = 0;
    if (x->x_chordlength > 100)
    {
    	x->x_chordlength = 0;
	clock_delay(x->x_clock, 500);
    }
    else clock_delay(x->x_clock, x->x_playtrans->t_delay);
    return;
fail:
    if (x->x_restart && x->x_firstpit >= 0)
    {
    	x->x_playpit = x->x_firstpit;
    	x->x_playtrans = 0;
    	clock_delay(x->x_clock, 500);
    }
}

static void snarkov_bang(t_snarkov *x)
{
    if (x->x_firstpit < 0)
    {
        int i, j;
        for (i = 0; i <DIM; i++)
            for (j = 0; j < DIM; j++)
                if (x->x_matrix[i][j].t_prob != 0)
        {
            x->x_firstpit = i;
            goto ok;
        }
	pd_error(x, "snarkov: no sequence");
	return;
    }
ok:
    x->x_playpit = x->x_firstpit;
    x->x_playtrans = 0;
    clock_delay(x->x_clock, 0);
}

static void snarkov_tick(t_snarkov *x)
{
    if (x->x_playtrans)
    {
    	outlet_float(x->x_durout, x->x_playtrans->t_length);
    	outlet_float(x->x_velout, x->x_playtrans->t_vel);
    	outlet_float(x->x_pitchout, x->x_playpit + FIRST);
    }
    else
    {
        outlet_float(x->x_durout, x->x_firstlength);
    	outlet_float(x->x_velout, x->x_firstvel);
    	outlet_float(x->x_pitchout, x->x_firstpit + FIRST);
    }
    snarkov_next(x);
}


static void snarkov_stop(t_snarkov *x)
{
    clock_unset(x->x_clock);
}

static void snarkov_resttime(t_snarkov *x, t_float f)
{
    x->x_resttime = f;
}

static void snarkov_restart(t_snarkov *x, t_float f)
{
    x->x_restart = f;
}

static void snarkov_maxdiff(t_snarkov *x, t_float f)
{
    x->x_maxdiff = f;
}

static void snarkov_write(t_snarkov *x, t_symbol *filename)
{
    FILE *fd;
    char buf[MAXPDSTRING];
    int i, j;
    canvas_makefilename(x->x_canvas, filename->s_name,
    	buf, MAXPDSTRING);
    sys_bashfilename(buf, buf);
    if (!(fd = fopen(buf, "w")))
    {
    	error("%s: can't create", buf);
    	return;
    }
    for (i = 0; i < DIM; i++)
    {
    	for (j = 0; j < DIM; j++)
	{
            if (x->x_matrix[i][j].t_prob <= 0)
                continue;
    	    if (fprintf(fd, "%5d %5d %5d %5d %5d %5d\n",
                i, j, 
    		(int)x->x_matrix[i][j].t_delay, 
    		(int)x->x_matrix[i][j].t_length, 
    		(int)x->x_matrix[i][j].t_prob, 
    		(int)x->x_matrix[i][j].t_vel) < 4) 
    	    {
    		error("%s: write error", filename->s_name);
    		goto fail;
    	    }
    	}
    }
fail:
    fclose(fd);
}

static void snarkov_read(t_snarkov *x, t_symbol *filename)
{
    int filedesc, i, j;
    FILE *fd;
    char buf[MAXPDSTRING], *bufptr;
    if ((filedesc = open_via_path(
    	canvas_getdir(x->x_canvas)->s_name,
    	    filename->s_name, "", buf, &bufptr, MAXPDSTRING, 0)) < 0)
    {
    	error("%s: can't open", filename->s_name);
    	return;
    }
    fd = fdopen(filedesc, "r");
    for (i = 0; i < DIM; i++)
    	for (j = 0; j < DIM; j++)
            x->x_matrix[i][j].t_delay = x->x_matrix[i][j].t_length = 
            x->x_matrix[i][j].t_prob = x->x_matrix[i][j].t_vel = 0;
    while (1)
    {
	int pit1, pit2;
	float delay, length, prob, vel;
    	if (fscanf(fd, "%d%d%f%f%f%f",
            &pit1, &pit2, &delay, &length, &prob, &vel) < 6)
                break;
    	if (pit1 < 0 || pit1 >= DIM || pit2 < 0 || pit2 > DIM
            || delay < 0 || length < 0 || prob < 0 || vel < 0)
        {
            post("snarkov: bad line (%d %d %f %f %f %f) dropped", 
                pit1, pit2, delay, length, prob, vel);
            continue;
        }
        x->x_matrix[pit1][pit2].t_delay = delay; 
        x->x_matrix[pit1][pit2].t_length = length; 
        x->x_matrix[pit1][pit2].t_prob = prob; 
        x->x_matrix[pit1][pit2].t_vel = vel; 
    }
    fclose(fd);
}

    /* old format - delete me */
static void snarkov_readold(t_snarkov *x, t_symbol *filename)
{
    int filedesc, i, j;
    FILE *fd;
    char buf[MAXPDSTRING], *bufptr;
    if ((filedesc = open_via_path(
    	canvas_getdir(x->x_canvas)->s_name,
    	    filename->s_name, "", buf, &bufptr, MAXPDSTRING, 0)) < 0)
    {
    	error("%s: can't open", filename->s_name);
    	return;
    }
    fd = fdopen(filedesc, "r");
    for (i = 0; i < DIM; i++)
    	for (j = 0; j < DIM; j++)
    {
    	if (fscanf(fd, "%f%f%f%f",
    	    &x->x_matrix[i][j].t_delay, 
    	    &x->x_matrix[i][j].t_length, 
    	    &x->x_matrix[i][j].t_prob, 
    	    &x->x_matrix[i][j].t_vel) < 4) 
    	{
    	    error("%s: read error", filename->s_name);
    	    break;
    	}
    }
    fclose(fd);
}

static void snarkov_print(t_snarkov *x)
{
    post("record %d restart %d maxdiff %f resttime %f",
    	x->x_record, x->x_restart, x->x_maxdiff,
    	x->x_resttime);
}

static void snarkov_free(t_snarkov *x)
{
    clock_free(x->x_clock);
}

static t_class *snarkov_class;

static void *snarkov_new(void)
{
    t_snarkov *x = (t_snarkov *)pd_new(snarkov_class);
    x->x_clock = clock_new(x, (t_method)snarkov_tick);
    floatinlet_new(&x->x_ob, &x->x_vel);
    x->x_pitchout = outlet_new(&x->x_ob, &s_float);
    x->x_velout = outlet_new(&x->x_ob, &s_float);
    x->x_durout = outlet_new(&x->x_ob, &s_float);
    x->x_restart = 0;
    x->x_resttime = 2000;
    x->x_maxdiff = 50;
    x->x_vel = 0;
    x->x_canvas = canvas_getcurrent();
    snarkov_clear(x);
    return (x);
}

void snarkov_setup(void)
{
    snarkov_class = class_new(gensym("snarkov"), (t_newmethod)snarkov_new,
    	(t_method)snarkov_free, sizeof(t_snarkov), 0, 0);
    class_addbang(snarkov_class, snarkov_bang);
    class_addfloat(snarkov_class, snarkov_float);
    class_addmethod(snarkov_class, (t_method)snarkov_print,
        gensym("print"), 0);
    class_addmethod(snarkov_class, (t_method)snarkov_clear,
        gensym("clear"), 0);
    class_addmethod(snarkov_class, (t_method)snarkov_record,
        gensym("record"), 0);
    class_addmethod(snarkov_class, (t_method)snarkov_stoprec,
        gensym("stoprec"), 0);
    class_addmethod(snarkov_class, (t_method)snarkov_stop,
        gensym("stop"), 0);
    class_addmethod(snarkov_class, (t_method)snarkov_resttime,
        gensym("resttime"), A_FLOAT, 0);
	    	/* historical synonym */
    class_addmethod(snarkov_class, (t_method)snarkov_resttime, gensym("set"),
    	A_FLOAT, 0);
    class_addmethod(snarkov_class, (t_method)snarkov_maxdiff, gensym("maxdiff"),
    	A_FLOAT, 0);
    class_addmethod(snarkov_class, (t_method)snarkov_restart, gensym("restart"),
    	A_FLOAT, 0);
    class_addmethod(snarkov_class, (t_method)snarkov_write, gensym("write"),
    	A_SYMBOL, 0);
    class_addmethod(snarkov_class, (t_method)snarkov_read, gensym("read"),
    	A_SYMBOL, 0);
    class_addmethod(snarkov_class, (t_method)snarkov_readold, gensym("readold"),
    	A_SYMBOL, 0);
}

