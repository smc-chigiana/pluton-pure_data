#include "m_pd.h"
#include <stdlib.h>
#include <stdio.h>

/* ragout: control 48 oscillators in a variety of ways specific to Pluton. */

#define NOSC 48
#define MODE_OFF 0
#define MODE_ADDPITCH 1
#define MODE_SKEW 11
#define MODE_GLISS 12
#define MODE_GLISS2 13
#define MODE_GLISS3 14

typedef struct _ragout
{
    t_object x_obj;
    void *x_out2;		/* outlet for frequencies */
    void *x_out1;   	    	/* outlet for index */
    void *x_clock;
    int x_mode;		    	/* what's happening */
    double x_starttime;	    	/* arpegio start time */
    float x_msec;    	    	/* arpegio time interval */
    int x_nosc;			/* number of oscilators */
    int x_count;		/* arpegio count */
    float x_pitches[NOSC];		/* pitches */
    float x_newpitches[NOSC];	/* new pitches (to gliss to) */
} t_ragout;

static void ragout_sendit(t_ragout *x, t_float f1, t_float f2)
{
    outlet_float(x->x_out2, f2);
    outlet_float(x->x_out1, f1);
}

    /* bang: output all pitches */
static void ragout_bang(t_ragout *x)
{
    int i, nosc = x->x_nosc;
    for (i = 0; i < nosc; i++)
    	ragout_sendit(x, i, x->x_pitches[i]);
}

    /* tick: various actions depending on mode */ 
static void ragout_tick(t_ragout *x)
{
    int i;
    int nosc = x->x_nosc;
    int count = x->x_count;
    float t = clock_gettimesince(x->x_starttime);
    float msec = x->x_msec, f1, f2;
    	/* "skew": arpeggiate current vector to output */
    if (x->x_mode == MODE_SKEW)
    {
	for (count = x->x_count; count < nosc; count++)
	{
	    if (x->x_pitches[count])
	    {
		ragout_sendit(x, count, x->x_pitches[count]);
		x->x_count = count + 1;
		clock_delay(x->x_clock, x->x_msec);
		return;
	    }
	    else ragout_sendit(x, count, 0L);
	}
    }
    else if (x->x_mode == MODE_GLISS)
    {
    	    /* glissando mode 1.  The oscillators gliss, all starting at once,
	    from the current values to the "new" ones.  The first one
	    reaches the target after "msec", the second one after 2*msec,
	    and so on, so that the arrivals are arpeggiated.    When the
	    last voice reaches its target, the targets become the "current"
	    pitches. */

	    /* First look for oscillators which have newly arrived at their 
	    new pitches: */
	while ((count < nosc) && (t > (count+1) * msec))
	{
	    if (!x->x_pitches[count])
	    	goto nomore;
	    ragout_sendit(x, count, x->x_newpitches[count]);
	    x->x_count = (++count);
	}

	    /* if we have reached an empty voice or the end of the list,
	    we're done.  */
	if ((count >= nosc) || (!x->x_pitches[count]))
	    goto nomore;

	    /* gliss all the ramaining oscillators, from count to the last
	    one. */
	while ((count < nosc) && (f1 = x->x_pitches[count]))
	{
	    if (f2 = x->x_newpitches[count])
	    {
		int totdel = (count+1) * msec;
		int freq = f1 + ((f2 - f1) * t)/totdel;
		ragout_sendit(x, count, freq);
	    }
	    else ragout_sendit(x, count, 0L);
	    count++;
	}
	clock_delay(x->x_clock, 25L);
    }
    else if (x->x_mode == MODE_GLISS2)
    {
    	    /* glissando mode 2.  The oscillators gliss one by one, each
    	    taking "msec" milliseconds.  */

    	    /* check whether the voice we're moving has arrived: */
	while ((count < nosc) && (t > (count+1) * msec))
	{
	    ragout_sendit(x, count, x->x_newpitches[count]);
	    x->x_count = ++count;
	}
	if ((count >= nosc) || (!x->x_pitches[count]))
	    goto nomore;
	f1 = x->x_pitches[count];
	if (f2 = x->x_newpitches[count])
	{
	    float begdel = count * msec;
	    float freq = f1 + ((f2 - f1) * (t - begdel))/msec;
	    ragout_sendit(x, count, freq);
	}
	else ragout_sendit(x, count, 0L);
	clock_delay(x->x_clock, 15L);
    }
    else if (x->x_mode == MODE_GLISS3)
    {
    	    /* glissando mode 3.  All the oscillators gliss at the same
	    time from the current values to the "new" ones.  */
	if (t >= msec)
	    goto nomore;
	for (count = 0; count < nosc; count++)
	{
	    float was = x->x_pitches[count], is = x->x_newpitches[count];
	    if (is != 0  && was != 0)
	    ragout_sendit(x, count,
		   was + (t * (x->x_newpitches[count] - was))/msec);
	}
	clock_delay(x->x_clock, 25L);
    }
    return;
nomore:
    for (i = 0; i < x->x_nosc; i++)
    	x->x_pitches[i] = x->x_newpitches[i];
}

static void ragout_skew(t_ragout *x, t_float f)
{
    if (f < 10) f = 10;
    x->x_msec = f;
    x->x_count = 0;
    x->x_mode = MODE_SKEW;
    ragout_tick(x);
}

static void ragout_slide(t_ragout *x, t_symbol *s, int ac, t_atom *av)
{
    int i, n, mode, msec;
    if (ac < 3 || av[0].a_type != A_FLOAT || av[1].a_type != A_FLOAT)
    {
	post("ragout: usage: gliss <mode> <msec> <pitch1> ...");
	return;
    }
    mode = atom_getfloatarg(0, ac, av);
    msec = atom_getfloatarg(1, ac, av);
    if (mode == 2) x->x_mode = MODE_GLISS2;
    else if (mode == 3)
    	x->x_mode = MODE_GLISS3;
    else x->x_mode = MODE_GLISS;
    if (msec < 10) 
    	msec = 10;
    x->x_msec = msec;
    x->x_count = 0;
    x->x_starttime = clock_getlogicaltime();
    ac -= 2; av += 2;
    n = ac;
    if (n > x->x_nosc)
    	n = x->x_nosc;
    for (i  = 0; i < n; i++)
    	x->x_newpitches[i] = atom_getfloat(av+i);
    for (i = n; i < x->x_nosc; i++)
    	x->x_newpitches[i] = 0;
    ragout_tick(x);
}

static void ragout_stop(t_ragout *x)
{
    clock_unset(x->x_clock);
    x->x_mode = MODE_OFF;
}

static void ragout_mode(t_ragout *x, t_floatarg f)
{
    if (f == 0)
    	x->x_mode = MODE_OFF;
    else if (f == 1)
    	x->x_mode = MODE_ADDPITCH;
    else error("ragout_mode: unsupported mode %f\n", f);
}

static void ragout_print(t_ragout *x)
{
    int i, n, nosc = x->x_nosc;

    post("ragout: mode %d:", x->x_mode);
    for (i = 0; i < nosc; i++)
    {
	if (x->x_pitches[i] != 0)
	    post("%d\t%d", i,  (int)(x->x_pitches[i]));
    };
}

static void ragout_set(t_ragout *x, t_symbol *s, int ac, t_atom *av)
{
    int nosc;
    int count;

    nosc = x->x_nosc;
    if (ac > nosc)
    	ac = nosc;
    for (count = 0; count < ac; count++)
    {
    	x->x_pitches[count] = atom_getfloat(av + count);
    }
    for (; count < nosc; count++)
    	x->x_pitches[count] = 0;
}

static void ragout_float(t_ragout *x, t_float f)
{
    int nosc = x->x_nosc;
    int count, hole = -1;

    if (x->x_mode == MODE_ADDPITCH)
    {
	for (count = 0; count < nosc; count++)
	    if (x->x_pitches[count] == f)
	    	return;
	for (count = 0; count < nosc; count++)
	    if (x->x_pitches[count] == 0)
	{
	    x->x_pitches[count] = f;
	    ragout_sendit(x, count, f);
    	    return;
	}
    }
}

static void ragout_free(t_ragout *x)
{
    clock_free(x->x_clock);
}

static t_class *ragout_class;

static void *ragout_new(t_float f)
{
    int i;
    t_ragout *x;
    int n = f;

    x = (t_ragout *)pd_new(ragout_class);
    if (n <= 0 || n > NOSC)
    	n = NOSC;
    x->x_out1 = outlet_new(&x->x_obj, &s_float);
    x->x_out2 = outlet_new(&x->x_obj, &s_float);
    x->x_clock = clock_new(&x->x_obj.ob_pd, (t_method)ragout_tick);
    x->x_mode = MODE_OFF;
    for (i = 0; i < NOSC; i++)
    	x->x_pitches[i] = x->x_newpitches[i] = 0;
    if (n < 0)
    	n = 0;
    else if (n > NOSC)
    	n = NOSC;
    x->x_nosc = n;
    return (x);
}

void ragout_setup( void)
{
    ragout_class = class_new(gensym("ragout"), (t_newmethod)ragout_new,
    	(t_method)ragout_free, sizeof(t_ragout), 0, A_DEFFLOAT, 0);
    class_addbang(ragout_class, ragout_bang);
    class_addfloat(ragout_class, ragout_float);

    class_addmethod(ragout_class, (t_method)ragout_print, gensym("print"), 0);
    class_addmethod(ragout_class, (t_method)ragout_stop, gensym("stop"), 0);

    class_addmethod(ragout_class, (t_method)ragout_set, gensym("set"), 
    	A_GIMME, 0);
    class_addmethod(ragout_class, (t_method)ragout_slide, gensym("slide"), 
    	A_GIMME, 0);
    class_addmethod(ragout_class, (t_method)ragout_mode, gensym("mode"), 
    	A_FLOAT, 0);
    class_addmethod(ragout_class, (t_method)ragout_skew, gensym("skew"), 
    	A_FLOAT, 0);
}
