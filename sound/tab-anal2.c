/* fourier analysis of 512-point 16-bit tables (nextstep format.) */

#include "math.h"
#include "stdio.h"

#define INTERLEAVE 1
#define NPOINTS 512
#define NPARTIAL 20

main(argc, argv)
 char **argv;
{
	short sbuf[NPOINTS * INTERLEAVE];
	float fbuf[NPOINTS];
	int i, j;
	float max = -1, min = 1;
	if (read(0, sbuf, 28) < 28)
	{
		fprintf(stderr, "read failed\n");
		exit(1);
	}
	if (read(0, sbuf, sizeof(sbuf)) < sizeof(sbuf))
	{
		fprintf(stderr, "read failed\n");
		exit(1);
	}
	for (i = 0; i < NPOINTS; i++)
	{
	    unsigned char *foo = (char *)&sbuf[i];
	    unsigned char a = foo[0], b = foo[1];
	    foo[0] = b;
	    foo[1] = a;
	}
	for (i = 0; i < NPOINTS; i++)
	{
		fbuf[i] = sbuf[INTERLEAVE * i ] * (1./32767.);
		if (fbuf[i] > max) max = fbuf[i];
		if (fbuf[i] < min) min = fbuf[i];
	}
	printf("[%f, %f]\n", min, max);
	for (j = 0; j < NPARTIAL; j++)
	{
		float sum1 = 0, sum2 = 0;
		for (i = i = 0; i < NPOINTS; i++)
		{
			sum1 += fbuf[i] * sin(i*j*2*3.14159/NPOINTS);
			sum2 += fbuf[i] * cos(i*j*2*3.14159/NPOINTS);
		}
		printf("%d %f %f\n", j, sum1/NPOINTS, sum2/NPOINTS);
	}
	exit(0);
}
