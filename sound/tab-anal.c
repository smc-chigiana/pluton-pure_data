#include "math.h"
#include "stdio.h"

#define INTERLEAVE 16
#define NPOINTS 4096
#define NPARTIAL 20

main(argc, argv)
 char **argv;
{
	short sbuf[NPOINTS * INTERLEAVE];
	float fbuf[NPOINTS];
	int i, j, onset;
	float max = -1, min = 1;
	if (argc < 2 || sscanf(argv[1], "%d", &onset) < 1)
	{
		fprintf(stderr, "neads onset argument\n");
		exit(1);
	}
	fprintf(stderr, "onset %d\n", onset);
	if (read(0, sbuf, sizeof(sbuf)) < sizeof(sbuf))
	{
		fprintf(stderr, "read failed\n");
		exit(1);
	}
	for (i = 0; i < NPOINTS; i++)
	{
		fbuf[i] = sbuf[INTERLEAVE * i + onset] * (1./32767.);
		if (fbuf[i] > max) max = fbuf[i];
		if (fbuf[i] < min) min = fbuf[i];
	}
	printf("[%f, %f]\n", min, max);
	for (j = 0; j < NPARTIAL; j++)
	{
		float sum1 = 0, sum2 = 0;
		for (i = i = 0; i < NPOINTS; i++)
		{
			sum1 += fbuf[i] * sin(i*j*2*3.14159/NPOINTS);
			sum2 += fbuf[i] * cos(i*j*2*3.14159/NPOINTS);
		}
		printf("%d %f %f\n", j, sum1/2048., sum2/2048.);
	}
	exit(0);
}
